const jwt = require('jsonwebtoken');

module.exports.createToken = (id, userName, userMail) => {
	let token = jwt.sign(
		{ id: id, userMail: userMail, userName: userName },
		process.env.JWT_SECRET,
		{
			expiresIn: 5 * 86400,
			algorithm: process.env.SIGNING_ALGO,
		}
	);

	return token;
};

module.exports.verifyToken = (token) => {
	let verified;
	try {
		verified = jwt.verify(token, process.env.JWT_SECRET);
		return { result: verified, error: null };
	}
	catch (err) {
		return { result: null, error: err };;
	}
};