const projectModel = require('../models/projectModel');
const userModel = require('../models/userModel');
const workItemModel = require('../models/workItemModel');
const prisma = require('../prisma/prisma');

const helper = require('../utils/helper');

exports.createWorkItem = async (req, res) => {
    const projectId = req.params.projectId;
    const { title, description, priority, statusId, assignedUserIds, userId, endDate } = req.body;

    try {
        // Check if the project exists
        const { result: project, error: projectError } = await projectModel.getProjectById(parseInt(projectId));

        if (projectError) {
            helper.handleError(
                res,
                `Error retrieving project: ${projectError}`,
                'getQueryError',
                500
            );
            return;
        }

        if (!project) {
            return helper.handleError(res,
                'Project not found!',
                'ProjectNotFound',
                404
            );
        }

        // Check if the assigned users exist
        const assignedUsers = await prisma.user.findMany({
            where: {
                id: { in: assignedUserIds },
            },
        });

        // Check if all assigned users are found
        if (assignedUsers.length !== assignedUserIds.length) {
            return helper.handleError(res,
                'Some assigned users not found',
                'UsersNotFound',
                404
            );
        }

        // Create the work item in the database
        const { result: newWorkItem, error: createdErr } = await workItemModel.createWorkItem(
            title, description, priority, statusId, parseInt(projectId),
            assignedUsers, userId, endDate
        )

        if (createdErr) {
            helper.handleError(
                res,
                `Internal Server Error : ${createdErr}`,
                'createdErr',
                500
            );
            return;
        }

        helper.handleSuccess(res,
            { workItem: newWorkItem },
            'Work item created successfully',
            201
        );

    } catch (error) {
        helper.handleError(res,
            'Internal server error',
            'InternalServerError',
            500
        );
    }
};

//	@route	PUT	/project/:projectId/workitem/:itemId
//	@desc		Update Work item
//	@body		title, description, endDate

exports.updateWorkItem = async (req, res) => {
    const { itemId } = req.params;
    const { title, description, endDate } = req.body;

    try {
        const { result: workItem, error: selectedErr } = await workItemModel.getWorkItemById(parseInt(itemId));

        if (!workItem) {
            helper.handleError(
                res,
                `Workitem not found!`,
                'NotFoundErr',
                500
            );
            return;
        }

        if (selectedErr) {
            helper.handleError(
                res,
                `Internal Server Error : ${selectedErr}`,
                'selectedErr',
                500
            );
            return;
        }

        const { result: updatedWorkItem, error: itemErr } = await workItemModel.updateWorkItemById(
            parseInt(itemId),
            {
                title,
                description,
                endDate
            }
        );

        if (itemErr) {
            helper.handleError(
                res,
                `Internal Server Error : ${itemErr}`,
                'itemErr',
                500
            );
            return;
        }

        helper.handleSuccess(
            res,
            { updatedWorkItem },
            `Successfully updated the workshop item!`,
            200
        );
    } catch (error) {
        helper.handleError(
            res,
            `Internal Server Error : ${error}`,
            'internalServerError',
            500
        );
    }
};

//	@route	PUT	/project/:projectId/workitem/:itemId/users
//	@desc		Update Work item users
//	@body		assignedUserIds

exports.updateWorkItemAssignedUsers = async (req, res) => {
    const { itemId } = req.params;
    const { assignedUserIds } = req.body;

    try {
        const { result: workItem, error: selectedErr } = await workItemModel.getWorkItemById(parseInt(itemId));

        if (!workItem) {
            helper.handleError(
                res,
                `Workitem not found!`,
                'NotFoundErr',
                500
            );
            return;
        }

        if (selectedErr) {
            helper.handleError(
                res,
                `Internal Server Error : ${selectedErr}`,
                'selectedErr',
                500
            );
            return;
        }

        const { result: updatedWorkItem, error: updateErr } = await workItemModel.updateWorkItemUsers(itemId, assignedUserIds);

        if (updateErr) {
            helper.handleError(
                res,
                `Updation Error : ${updateErr}`,
                'updateErr',
                500
            );
            return;
        }

        helper.handleSuccess(
            res,
            { workItem: updatedWorkItem },
            'Work item assigned users updated successfully',
            200
        );

    } catch (error) {
        helper.handleError(
            res,
            `Error updating work item assigned users: ${error.message}`,
            'UpdateWorkItemAssignedUsersError',
            500
        );
    }
};

//	@route	DELETE	/project/:projectId/workitem/:itemId
//	@desc		Delete Work item
//	@body		None

exports.deleteWorkItem = async (req, res) => {
    const itemId = req.params.itemId;

    try {
        const { result: deletedItem, error: itemErr } = await workItemModel.deleteWorkItemById(parseInt(itemId));

        if (itemErr) {
            helper.handleError(
                res,
                `Internal Server Error : ${itemErr}`,
                'itemErr',
                500
            );
            return;
        }

        helper.handleSuccess(
            res,
            { deletedItem },
            `Successfully deleted the project!`,
            200
        );
    } catch (error) {
        helper.handleError(
            res,
            `Internal Server Error : ${error}`,
            'internalServerError',
            500
        );
    }
};