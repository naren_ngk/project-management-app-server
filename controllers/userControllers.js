const userModel = require('../models/userModel');
const hash = require('../utils/hashPassword');
const jwtAuth = require('../services/jwtAuth');
const helper = require('../utils/helper');

//	@route	POST	/login
//	@desc		Login Existing User
//	@body		userMail, Password

exports.loginUser = async (req, res) => {
    const { userMail, password } = req.body;

    try {
        // Retrieve user's password from database
        const { result: user, error: userError } = await userModel.getUserByEmail(userMail);

        if (userError) {
            helper.handleError(
                res,
                `Error retrieving user: ${userError}`,
                'userQueryError',
                500
            );
            return;
        }

        if (!user) {
            helper.handleError(
                res,
                `User with email ${userMail} not found`,
                'userNotFound',
                404
            );
            return;
        }

        // Verify the password
        const isMatch = hash.verifyPassword(password, user.password);

        if (!isMatch) {
            helper.handleError(
                res,
                `Incorrect password for user ${userMail}`,
                'incorrectPassword',
                401
            );
            return;
        }

        // Create JWT token
        const token = jwtAuth.createToken(user.id, userMail, user.username);

        // Send success response with token
        helper.handleSuccess(
            res,
            { token },
            `User ${user.username} successfully logged in`,
            200
        );
    }
    catch (error) {
        helper.handleError(
            res,
            `Internal Server Error: ${error.message}`,
            'internalServerError',
            500
        );
    }
};

//	@route	POST	/register
//	@desc		Register New User
//	@body		userName, email, password

exports.registerUser = async (req, res) => {
	const { username, email, password, role } = req.body;

	// Check whether user already exist
	const { result: selectedUser, error: selectedErr } =
		await userModel.getUserByEmail(email);

	if (selectedErr) {
		helper.handleError(
			res,
			`Internal Server Error : ${selectedErr}`,
			'SelectedErr',
			500
		);
		return;
	}

	if (selectedUser) {
		helper.handleError(
			res,
			`User already exists ${selectedUser}`,
			'selectedUser',
			400
		);
		return;
	}

	// Create hashed password
	const passwordHashed = hash.hashPassword(password);

	// Create new User
	const { result: createdUser, error: createdErr } = await userModel.createUser(
		username,
		email,
		passwordHashed,
		role
	);

	if (createdErr) {
		helper.handleError(
			res,
			`Internal Server Error : ${createdErr}`,
			'createdErr',
			500
		);
		return;
	}

    helper.handleSuccess(
		res,
		{ createdUser },
		`Successfully Saved the User details : ${username}`,
		201
	);
	return;
};