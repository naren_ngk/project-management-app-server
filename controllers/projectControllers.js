const projectModel = require('../models/projectModel');
const userModel = require('../models/userModel');
const prisma = require('../prisma/prisma');
const helper = require('../utils/helper');


//	@route	POST	/project/new
//	@desc		Create New Project
//	@body		projectName, description, startDate, endDate, createdByUserId

exports.createProject = async (req, res) => {
    const { projectName, description, startDate, endDate, createdByUserId, assignedUserIds } = req.body;

    try {
        const assignedUsers = await prisma.user.findMany({
            where: {
                id: { in: assignedUserIds },
            },
        });

        if (assignedUsers.length !== assignedUserIds.length) {
            return helper.handleError(res,
                'Some assigned users not found',
                'UsersNotFound',
                404
            );
        }

        const { result: selectedUser, error: selectedErr } =
            await userModel.getUserById(createdByUserId);

        if (selectedErr) {
            helper.handleError(
                res,
                `Internal Server Error : ${projectErr}`,
                'ProjectErr',
                500
            );
            return;
        }

        if (!selectedUser) {
            return helper.handleError(res,
                'Admin user not found',
                'UsersNotFound',
                404
            );
        }

        const { result: project, error: projectErr } = await projectModel.createProject(
            projectName,
            description,
            startDate,
            endDate,
            createdByUserId,
            assignedUserIds
        );

        if (projectErr) {
            helper.handleError(
                res,
                `Internal Server Error : ${projectErr}`,
                'ProjectErr',
                500
            );
            return;
        }

        helper.handleSuccess(
            res,
            { project },
            `Successfully cretated the project : ${projectName}`,
            201
        );
    }
    catch (error) {
        helper.handleError(
            res,
            `Internal Server Error : ${error}`,
            'internalServerError',
            500
        );
    }
};

//	@route	PUT	/project/:projectId
//	@desc		Update Project
//	@body		projectName, description, endDate

exports.updateProject = async (req, res) => {
    const projectId = req.params.projectId;
    const { name, description, endDate } = req.body;

    try {
        const { result: updatedProject, error: projectErr } = await projectModel.updateProjectById(
            parseInt(projectId),
            {
                name,
                description,
                endDate
            }
        );

        if (projectErr) {
            helper.handleError(
                res,
                `Internal Server Error : ${projectErr}`,
                'ProjectErr',
                500
            );
            return;
        }

        helper.handleSuccess(
            res,
            { updatedProject },
            `Successfully updated the project: ${name}`,
            200
        );
    } catch (error) {
        helper.handleError(
            res,
            `Internal Server Error : ${error}`,
            'internalServerError',
            500
        );
    }
};

//	@route	DELETE	/project/:projectId
//	@desc		Delete Project
//	@body		None

exports.deleteProject = async (req, res) => {
    const projectId = req.params.projectId;

    try {
        const { result: deletedProject, error: projectErr } = await projectModel.deleteProjectById(parseInt(projectId));

        if (projectErr) {
            helper.handleError(
                res,
                `Internal Server Error : ${projectErr}`,
                'ProjectErr',
                500
            );
            return;
        }

        helper.handleSuccess(
            res,
            { deletedProject },
            `Successfully deleted the project!`,
            200
        );
    } catch (error) {
        helper.handleError(
            res,
            `Internal Server Error : ${error}`,
            'internalServerError',
            500
        );
    }
};
