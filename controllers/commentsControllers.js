const commentModel = require('../models/commentModel');
const helper = require('../utils/helper');
const userModel = require('../models/userModel');
const workItemModel = require('../models/workItemModel');

// Create a new comment
exports.createComment = async (req, res) => {
    const { content, userId, workItemId } = req.body;

    try {
        const { result: user, error: userError } = await userModel.getUserById(userId);

        if (userError) {
            helper.handleError(
                res,
                `Internal Server Error : ${userError}`,
                'userError',
                500
            );
            return;
        }

        if (!user) {
            helper.handleError(
                res,
                `User does not exists!`,
                'selectedUser',
                400
            );
            return;
        }

        const { result: workItem, error: workItemErr } = await workItemModel.getWorkItemById(parseInt(workItemId));

        if (!workItem) {
            helper.handleError(
                res,
                `Workitem not found!`,
                'NotFoundErr',
                500
            );
            return;
        }

        if (workItemErr) {
            helper.handleError(
                res,
                `Internal Server Error : ${workItemErr}`,
                'workItemErr',
                500
            );
            return;
        }

        const { result: comment, error } = await commentModel.createComment(
            content,
            userId,
            workItemId
        );

        if (error) {
            helper.handleError(
                res,
                `Internal Server Error: ${error}`,
                'CreateCommentError',
                500
            );
            return;
        }

        helper.handleSuccess(
            res,
            { comment },
            'Comment created successfully.',
            201
        );
    } catch (err) {
        helper.handleError(
            res,
            `Internal Server Error: ${err}`,
            'InternalError',
            500
        );
    }
};

// Update a comment by ID
exports.updateCommentById = async (req, res) => {
    const { commentId } = req.params;
    const { content } = req.body;

    try {
        const { result: comment, error } = await commentModel.updateCommentById(parseInt(commentId), { content });

        if (error) {
            helper.handleError(
                res,
                `Internal Server Error: ${error}`,
                'UpdateCommentByIdError',
                500
            );
            return;
        }

        if (!comment) {
            helper.handleError(
                res,
                'Comment not found.',
                'CommentNotFoundError',
                404
            );
            return;
        }

        helper.handleSuccess(
            res,
            { comment },
            'Comment updated successfully.',
            200
        );
    } catch (err) {
        helper.handleError(
            res,
            `Internal Server Error: ${err}`,
            'InternalError',
            500
        );
    }
};

// Delete a comment by ID
exports.deleteCommentById = async (req, res) => {
    const { commentId } = req.params;

    try {
        const { result: comment, error } = await commentModel.deleteCommentById(parseInt(commentId));

        if (error) {
            helper.handleError(
                res,
                `Internal Server Error: ${error}`,
                'DeleteCommentByIdError',
                500
            );
            return;
        }

        if (!comment) {
            helper.handleError(
                res,
                'Comment not found.',
                'CommentNotFoundError',
                404

            );
            return;
        }

        helper.handleSuccess(
            res,
            { comment },
            'Comment deleted successfully.',
            200
        );
    } catch (err) {
        helper.handleError(
            res,
            `Internal Server Error: ${err}`,
            'InternalError',
            500
        );
    }
};
