# Project Overview

This project aims to develop a comprehensive web application for managing projects and facilitating team collaboration within an organization. The application will provide functionalities for user authentication and authorization, project management, work item management, work item status tracking, and team communication. 

## Functional Requirements

### User Authentication and Authorization

- **User Login and Logout:** Users should be able to log in to and log out of the application securely.
- **Role-Based Access Control (RBAC):** Different user roles will have different levels of access:
  - **Super Admin:** Has full access to all features and functionalities of the application, including user and project management. Can create admins and other users.
  - **Admin:** Can monitor activities, create projects, project managers, and developers. Can assign project managers to projects.
  - **Project Manager:** Can create, update, delete, and manage work items within projects. Can assign work items to developers.
  - **Developers:** Can view projects they are assigned to, manage work items assigned to them, and communicate with other team members.

### Project Management

- **Project Creation:** Admins can create new projects, specifying project details such as name, description, start date, and end date.
- **Work Item Association:** Projects can have multiple work items associated with them.
- **User Project View:** Users can view a list of projects they are involved in and their details.

### Work Item Management

- **Work Item Creation:** Work items can be created within a project, including details such as title, description, priority, status, and assigned team member(s).
- **Assignment:** Work items can be assigned to one or more team members.
- **User Work Item View:** Users can view a list of work items assigned to them and their details.
- **Status Management:** Work items should have statuses like "New", "Active", "Resolved", and "Archived".

### Work Item Status Tracking

- **Status Update:** Users can update work item status or reassign work items to other team members.
- **Progress Tracking:** Project managers should be able to track the overall progress of work items within each project.

### Team Communication

- **In-App Communication:** Users can communicate within the application through features like comments on work items or project-wide announcements.
- **Notifications:** Users receive notifications (both in-app and email) when assigned a new task, mentioned in a comment, or when there are updates to work items they are involved in.
- **Real-Time Communication:** In-app notifications should be delivered in real-time using WebSockets for efficient communication.
- **Email Notifications:** A message broker should be used to queue and reliably deliver email notifications.

## Tech Stack


-  [Node.js](https://nodejs.org/en/)
-  [Express](https://expressjs.com/)
-  [PostgreSQL](https://www.postgresql.org/)
-  [Prisma ORM](https://www.prisma.io/)
-  [JWT](https://jwt.io/)
-  [Bcrypt](https://www.npmjs.com/package/bcrypt)
-  [Joi](https://www.npmjs.com/package/joi)


## Prerequisites

Make sure you have the following dependencies installed on your machine:

-  Node.js - [Download and install Node.js](https://nodejs.org/en/download/)
-  PostgreSQL - [Download and install PostgreSQL](https://www.postgresql.org/download/)

## Getting Started

1. Clone this repository to your local machine using the following command:

   ```bash
   git clone https://gitlab.com/naren_ngk/project-management-app-server <project_name>

   ```

2. Navigate to the project directory:

   ```bash
   cd <project_name>
   ```

3. Install the dependencies:

   ```bash
      npm install
   ```

4. Create a new PostgreSQL database for your project.
5. Modify env files in the root directory of the project.

6. Modify the `schema.prisma` file in the `prisma` directory to match your database schema.

7. Run the following command to generate the Prisma client:

   ```bash
   npx prisma generate
   ```

8. Run the following command to start the development server:

   ```bash
   npm run dev
   ```
   
The server should now be running at http://localhost:5000.


## Happy Coding!