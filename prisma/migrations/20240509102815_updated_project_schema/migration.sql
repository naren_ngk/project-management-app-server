-- CreateTable
CREATE TABLE "_assignedProjects" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_assignedProjects_AB_unique" ON "_assignedProjects"("A", "B");

-- CreateIndex
CREATE INDEX "_assignedProjects_B_index" ON "_assignedProjects"("B");

-- AddForeignKey
ALTER TABLE "_assignedProjects" ADD CONSTRAINT "_assignedProjects_A_fkey" FOREIGN KEY ("A") REFERENCES "Project"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_assignedProjects" ADD CONSTRAINT "_assignedProjects_B_fkey" FOREIGN KEY ("B") REFERENCES "User"("id") ON DELETE CASCADE ON UPDATE CASCADE;
