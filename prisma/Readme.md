# Project Prisma Schema

This project utilizes Prisma as the ORM (Object-Relational Mapping) tool for interacting with the database. Prisma allows seamless integration with various databases, providing a powerful and intuitive way to define data models and perform database operations.

### User Model
- Stores user details such as username, email, password, and role.
- Users can be assigned to projects, work items, and can create comments and notifications.

### Project Model
- Represents a project with attributes like name, description, start date, and end date.
- Each project is associated with an admin user and can have multiple work items.

### Work Item Model
- Describes a task within a project, including title, description, priority, status, and assigned team member(s).
- Work items can be assigned to one or more users and belong to a specific project.
- Tracks creation date, end date, and comments associated with the work item.

### Status Model
- Defines different statuses that work items can have, such as "New", "Active", "Resolved", etc.

### Comment Model
- Represents a comment made by a user on a specific work item.
- Stores the content of the comment, the user who made it, and the associated work item.

### Notification Model
- Stores notifications for users, such as new task assignments or updates to work items.
- Includes content, type, and the user receiving the notification.

This schema provides a structured way to organize and manage project-related data, facilitating efficient project management and team collaboration.

## Database Configuration

### Database Provider

This project uses PostgreSQL as the database provider. PostgreSQL is a robust, open-source relational database management system known for its reliability and scalability.

### Database URL

The database connection URL is provided via an environment variable `DATABASE_URL`. Make sure to set this environment variable with the appropriate PostgreSQL connection string.

```env
DATABASE_URL=postgresql://user:password@localhost:5432/database_name
```

## Prisma Schema

The Prisma schema defines the data models used in the application, including users, projects, work items, statuses, comments, and notifications. It also specifies the relationships between these models.

```prisma
generator client {
  provider = "prisma-client-js"
}

datasource db {
  provider = "postgresql"
  url      = env("DATABASE_URL")
}

```

## Usage

### Installation

To install Prisma and generate the Prisma Client:

```bash
npm install @prisma/cli
```

### Prisma CLI Commands

- **Migrate Database:** Apply database migrations to sync your Prisma schema with the actual database schema.
  ```bash
  npx prisma migrate dev
  ```

- **Generate Prisma Client:** Generate the Prisma Client for TypeScript or JavaScript.
  ```bash
  npx prisma generate
  ```

- **Seed Database (Optional):** Seed the database with initial data.
  ```bash
  npx prisma db seed
  ```

### Using Prisma Client

Once the Prisma Client is generated, you can use it in your application to perform database queries and mutations. Here's a basic example of connecting to your database using Prisma:

```javascript
const { PrismaClient } = require('@prisma/client');

const prisma = new PrismaClient({
   datasources: {
      db: {
         url: process.env.DATABASE_URL,
      },
   },
});

prisma
   .$connect()
   .then(() => {
      console.log('Database connected successfully');
   })
   .catch((err) => {
      console.log('Database connection failed');
      console.log(err);
   });

module.exports = prisma;
```

For more information on how to use Prisma, refer to the [Prisma documentation](https://www.prisma.io/docs).