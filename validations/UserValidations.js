const joi = require('joi');
const helper = require('../utils/helper');

const signInSchema = joi.object({
    email: joi.string().email().required().messages({
        'string.empty': 'Email is required.',
        'string.base': 'Email must be a string.',
        'string.email': 'Invalid email format. Please provide a valid email address.',
        'any.required': 'Email is required.',
    }),
    password: joi.string().pattern(new RegExp('^[a-zA-Z0-9]{7,30}$')).required().messages({
        'string.base': 'Password must be a string.',
        'string.empty': 'Password is required.',
        'string.pattern.base': 'Password must be between 7 to 30 characters long and contain only alphanumeric characters.',
        'any.required': 'Password is required.',
    }),
});

const signUpSchema = joi.object({
    username: joi.string().required().messages({
        'string.empty': 'Username is required.',
        'string.base': 'Username must be a string.',
        'any.required': 'Username is required.',
    }),
    email: joi.string().email().required().messages({
        'string.empty': 'Email is required.',
        'string.base': 'Email must be a string.',
        'string.email': 'Invalid email format. Please provide a valid email address.',
        'any.required': 'Email is required.',
    }),
    password: joi.string().pattern(new RegExp('^[a-zA-Z0-9]{7,30}$')).required().messages({
        'string.base': 'Password must be a string.',
        'string.empty': 'Password is required.',
        'string.pattern.base': 'Password must be between 7 to 30 characters long and contain only alphanumeric characters.',
        'any.required': 'Password is required.',
    }),
    role: joi.string().valid('SUPER_ADMIN', 'ADMIN', 'PROJECT_MANAGER', 'DEVELOPER').required().messages({
        'string.empty': 'Role is required.',
        'string.base': 'Role must be a string.',
        'any.required': 'Role is required.',
        'any.only': 'Invalid role. Role must be one of: SUPER_ADMIN, ADMIN, PROJECT_MANAGER, DEVELOPER.',
    }),
});

module.exports.signInValidation = (req, res, next) => {
	const { validationError } = signInSchema.validate(req.body);
	if (validationError) {
		helper.handleError(
			res,
			`Validation error : ${validationError}`,
			'selectedUserErr',
			500
		);
		return;
	}
	next();
};

module.exports.signUpValidation = (req, res, next) => {
	const { validationError } = signUpSchema.validate(req.body);
	if (validationError) {
		helper.handleError(
			res,
			`Validation error : ${validationError}`,
			'selectedUserErr',
			500
		);
		return;
	}
	next();
};