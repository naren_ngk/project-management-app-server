const Joi = require('joi');

const createCommentSchema = Joi.object({
    content: Joi.string().required().messages({
        'string.empty': 'Content is required.',
        'any.required': 'Content is required.',
    }),
    userId: Joi.number().integer().positive().required().messages({
        'number.base': 'User ID must be a number.',
        'number.integer': 'User ID must be an integer.',
        'number.positive': 'User ID must be a positive number.',
        'any.required': 'User ID is required.',
    }),
    workItemId: Joi.number().integer().positive().required().messages({
        'number.base': 'Work Item ID must be a number.',
        'number.integer': 'Work Item ID must be an integer.',
        'number.positive': 'Work Item ID must be a positive number.',
        'any.required': 'Work Item ID is required.',
    }),
});

const updateCommentSchema = Joi.object({
    content: Joi.string().required().messages({
        'string.empty': 'Content is required.',
        'any.required': 'Content is required.',
    }),
});

module.exports.createCommentValidation = (req, res, next) => {
    const { validationError } = createCommentSchema.validate(req.body);
    if (validationError) {
        helper.handleError(
            res,
            `Validation error : ${validationError}`,
            'selectedUserErr',
            500
        );
        return;
    }
    next();
};

module.exports.updateCommentValidation = (req, res, next) => {
    const { validationError } = updateCommentSchema.validate(req.body);
    if (validationError) {
        helper.handleError(
            res,
            `Validation error : ${validationError}`,
            'selectedUserErr',
            500
        );
        return;
    }
    next();
};