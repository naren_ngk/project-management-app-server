const Joi = require('joi');

const createProjectSchema = Joi.object({
    projectName: Joi.string().required().messages({
        'any.required': 'Project name is required.',
        'string.empty': 'Project name cannot be empty.',
    }),
    description: Joi.string().required().messages({
        'any.required': 'Description is required.',
        'string.empty': 'Description cannot be empty.',
    }),
    startDate: Joi.date().iso().required().messages({
        'any.required': 'Start date is required.',
        'date.base': 'Start date must be a valid date in ISO format.',
    }),
    endDate: Joi.date().iso().required().messages({
        'any.required': 'End date is required.',
        'date.base': 'End date must be a valid date in ISO format.',
    }),
    createdByUserId: Joi.number().integer().required().messages({
        'any.required': 'Created by user ID is required.',
        'number.base': 'Created by user ID must be a valid number.',
        'number.integer': 'Created by user ID must be an integer.',
    }),
    assignedUsers: Joi.array().items(Joi.number().integer().positive()).min(1).required().messages({
        'array.base': 'Assigned User IDs must be an array of numbers',
        'array.min': 'At least one assigned user ID is required',
        'number.base': 'Each assigned user ID must be a number',
        'number.integer': 'Each assigned user ID must be an integer',
        'number.positive': 'Each assigned user ID must be a positive number',
        'any.required': 'Assigned User IDs are required',
    }),
});

const updateProjectSchema = Joi.object({
    projectName: Joi.string().messages({
        'string.empty': 'Project name cannot be empty.',
    }),
    description: Joi.string().messages({
        'string.empty': 'Description cannot be empty.',
    }),
    endDate: Joi.date().iso().messages({
        'date.base': 'End date must be a valid date in ISO format.',
    }),
});

module.exports.createProjectValidation = (req, res, next) => {
    const { validationError } = createProjectSchema.validate(req.body);
    if (validationError) {
        helper.handleError(
            res,
            `Validation error : ${validationError}`,
            'selectedUserErr',
            500
        );
        return;
    }
    next();
};

module.exports.updateProjectValidation = (req, res, next) => {
    const { validationError } = updateProjectSchema.validate(req.body);
    if (validationError) {
        helper.handleError(
            res,
            `Validation error : ${validationError}`,
            'selectedUserErr',
            500
        );
        return;
    }
    next();
};