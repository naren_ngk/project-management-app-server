const Joi = require('joi');

const createWorkItemSchema = Joi.object({
    title: Joi.string().min(1).max(100).required().messages({
        'string.base': 'Title must be a string',
        'string.empty': 'Title is required',
        'any.required': 'Title is required',
    }),
    description: Joi.string().max(500).allow('').optional().messages({
        'string.base': 'Description must be a string',
        'string.max': 'Description cannot exceed {#limit} characters',
    }),
    priority: Joi.string().valid('low', 'medium', 'high').required().messages({
        'string.base': 'Priority must be a string',
        'any.only': 'Priority must be one of "low", "medium", or "high"',
        'any.required': 'Priority is required',
    }),
    statusId: Joi.number().integer().positive().required().messages({
        'number.base': 'Status ID must be a number',
        'number.integer': 'Status ID must be an integer',
        'number.positive': 'Status ID must be a positive number',
        'any.required': 'Status ID is required',
    }),
    userId: Joi.number().integer().positive().required().messages({
        'number.base': 'Status ID must be a number',
        'number.integer': 'Status ID must be an integer',
        'number.positive': 'Status ID must be a positive number',
        'any.required': 'Status ID is required',
    }),
    assignedUserIds: Joi.array().items(Joi.number().integer().positive()).min(1).required().messages({
        'array.base': 'Assigned User IDs must be an array of numbers',
        'array.min': 'At least one assigned user ID is required',
        'number.base': 'Each assigned user ID must be a number',
        'number.integer': 'Each assigned user ID must be an integer',
        'number.positive': 'Each assigned user ID must be a positive number',
        'any.required': 'Assigned User IDs are required',
    }),
    endDate: Joi.date().iso().required().messages({
        'date.base': 'End date must be a valid date in ISO format.',
    }),
});

const updateWorkItemSchema = Joi.object({
    title: Joi.string().min(1).max(100).required().messages({
        'string.base': 'Title must be a string',
        'string.empty': 'Title is required',
        'any.required': 'Title is required',
    }),
    description: Joi.string().max(500).allow('').optional().messages({
        'string.base': 'Description must be a string',
        'string.max': 'Description cannot exceed {#limit} characters',
    }),
    endDate: Joi.date().iso().required().messages({
        'date.base': 'End date must be a valid date in ISO format.',
    }),
});

const updateWorkItemUsersSchema = Joi.object({
    assignedUserIds: Joi.array().items(Joi.number().integer().positive()).min(1).required().messages({
        'array.base': 'Assigned User IDs must be an array of numbers',
        'array.min': 'At least one assigned user ID is required',
        'number.base': 'Each assigned user ID must be a number',
        'number.integer': 'Each assigned user ID must be an integer',
        'number.positive': 'Each assigned user ID must be a positive number',
        'any.required': 'Assigned User IDs are required',
    }),
});

module.exports.createWorkItemValidation = (req, res, next) => {
    const { validationError } = createWorkItemSchema.validate(req.body);
    if (validationError) {
        helper.handleError(
            res,
            `Validation error : ${validationError}`,
            'selectedUserErr',
            500
        );
        return;
    }
    next();
};

module.exports.updateWorkItemValidation = (req, res, next) => {
    const { validationError } = updateWorkItemSchema.validate(req.body);
    if (validationError) {
        helper.handleError(
            res,
            `Validation error : ${validationError}`,
            'selectedUserErr',
            500
        );
        return;
    }
    next();
};

module.exports.updateWorkItemUserValidation = (req, res, next) => {
    const { validationError } = updateWorkItemUsersSchema.validate(req.body);
    if (validationError) {
        helper.handleError(
            res,
            `Validation error : ${validationError}`,
            'selectedUserErr',
            500
        );
        return;
    }
    next();
};