const User = require("../models/userModel");

const { handleError } = require("../utils/helper");

const verifyUserRole = (specifiedRole) => async (req, res, next) => {
    const userId = req.body.userId;
    try {
        const { result: user, error: userError } = await User.getUserById(userId);

        if (!user) {
            handleError(res, "User not found.", 404);
            return;
        }

        if (userError) {
            handleError(res, `Internal Server error: ${userError}`, "InternalError", 500);
            return;
        }

        const hasRole = user.role === specifiedRole;

        if (hasRole) {
            req.user = user;
            next();
        } else {
            handleError(res, "You do not have permission to access this route.", 'Forbidden Action!', 403);
        }
    } catch (err) {
        handleError(res, err.message, 500);
    }
};

module.exports = verifyUserRole;