const prisma = require('../prisma/prisma');

// Create a new status
exports.createStatus = async (name) => {
    try {
        const status = await prisma.status.create({
            data: {
                name,
            },
        });

        return { result: status, error: null };
    } catch (err) {
        return { result: null, error: err.message };
    }
};

// Retrieve a status by ID
exports.getStatusById = async (statusId) => {
    try {
        const status = await prisma.status.findUnique({
            where: {
                id: statusId,
            },
        });

        return { result: status, error: null };
    } catch (err) {
        return { result: null, error: err.message };
    }
};

// Update a status by ID
exports.updateStatusById = async (statusId, name) => {
    try {
        const status = await prisma.status.update({
            where: {
                id: statusId,
            },
            data: {
                name,
            },
        });

        return { result: status, error: null };
    } catch (err) {
        return { result: null, error: err.message };
    }
};

// Delete a status by ID
exports.deleteStatusById = async (statusId) => {
    try {
        const status = await prisma.status.delete({
            where: {
                id: statusId,
            },
        });

        return { result: status, error: null };
    } catch (err) {
        return { result: null, error: err.message };
    }
};
