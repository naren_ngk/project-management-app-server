const prisma = require('../prisma/prisma');

// Create a new notification
exports.createNotification = async (content, type, userId) => {
    try {
        const notification = await prisma.notification.create({
            data: {
                content,
                type,
                userId,
            },
        });

        return { result: notification, error: null };
    } catch (err) {
        return { result: null, error: err.message };
    }
};

// Retrieve all notifications
exports.getAllNotifications = async () => {
    try {
        const notifications = await prisma.notification.findMany();
        return { result: notifications, error: null };
    } catch (err) {
        return { result: null, error: err.message };
    }
};

// Retrieve a notification by ID
exports.getNotificationById = async (notificationId) => {
    try {
        const notification = await prisma.notification.findUnique({
            where: {
                id: notificationId,
            },
        });

        return { result: notification, error: null };
    } catch (err) {
        return { result: null, error: err.message };
    }
};

// Update a notification by ID
exports.updateNotificationById = async (notificationId, data) => {
    try {
        const notification = await prisma.notification.update({
            where: {
                id: notificationId,
            },
            data,
        });

        return { result: notification, error: null };
    } catch (err) {
        return { result: null, error: err.message };
    }
};

// Delete a notification by ID
exports.deleteNotificationById = async (notificationId) => {
    try {
        const notification = await prisma.notification.delete({
            where: {
                id: notificationId,
            },
        });

        return { result: notification, error: null };
    } catch (err) {
        return { result: null, error: err.message };
    }
};
