const prisma = require('../prisma/prisma');

// Create a new work item
exports.createWorkItem = async (title, description, priority, statusId, projectId, assignedUsers, userId, endDate) => {
    try {
        const workItem = await prisma.workItem.create({
            data: {
                title,
                description,
                priority,
                endDate,
                status: {
                    connect: { id: statusId },
                },
                project: {
                    connect: { id: projectId },
                },
                assignedTo: {
                    connect: assignedUsers.map(user => ({ id: user.id })),
                },
                createdBy: {
                    connect: { id: userId }
                }
            },
            include: {
                status: true,
                assignedTo: true,
                project: true,
                createdBy: true,
            },
        });

        return { result: workItem, error: null };
    } catch (err) {
        return { result: null, error: err };
    }
};

// Retrieve all work items
exports.getAllWorkItems = async () => {
    try {
        const workItems = await prisma.workItem.findMany();
        return { result: workItems, error: null };
    } catch (err) {
        return { result: null, error: err };
    }
};

// Retrieve a work item by ID
exports.getWorkItemById = async (workItemId) => {
    try {
        const workItem = await prisma.workItem.findUnique({
            where: {
                id: workItemId,
            },
        });

        return { result: workItem, error: null };
    } catch (err) {
        return { result: null, error: err };
    }
};

// Update a work item by ID
exports.updateWorkItemById = async (workItemId, data) => {
    try {
        const workItem = await prisma.workItem.update({
            where: {
                id: workItemId,
            },
            data,
        });

        return { result: workItem, error: null };
    } catch (err) {
        return { result: null, error: err };
    }
};

exports.updateWorkItemUsers = async (workItemId, users) => {
    try {
        const updatedWorkItem = await prisma.workItem.update({
            where: { id: parseInt(workItemId) }, 
            data: {
                assignedTo: { 
                    set: users.map(userId => ({ id: parseInt(userId) })),
                },
            },
            include: { assignedTo: true },
        });

        return { result: updatedWorkItem, error: null };
    } catch (err) {
        return { result: null, error: err };
    }
}

// Delete a work item by ID
exports.deleteWorkItemById = async (workItemId) => {
    try {
        const workItem = await prisma.workItem.delete({
            where: {
                id: workItemId,
            },
        });

        return { result: workItem, error: null };
    } catch (err) {
        return { result: null, error: err };
    }
};
