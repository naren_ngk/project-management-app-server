const prisma = require('../prisma/prisma');

// Create a new comment
exports.createComment = async (content, userId, workItemId) => {
    try {
        const comment = await prisma.comment.create({
            data: {
                content,
                userId,
                workItemId,
            },
        });

        return { result: comment, error: null };
    } catch (err) {
        return { result: null, error: err.message };
    }
};

// Retrieve all comments
exports.getAllComments = async () => {
    try {
        const comments = await prisma.comment.findMany();
        return { result: comments, error: null };
    } catch (err) {
        return { result: null, error: err.message };
    }
};

// Retrieve a comment by ID
exports.getCommentById = async (commentId) => {
    try {
        const comment = await prisma.comment.findUnique({
            where: {
                id: commentId,
            },
        });

        return { result: comment, error: null };
    } catch (err) {
        return { result: null, error: err.message };
    }
};

// Update a comment by ID
exports.updateCommentById = async (commentId, data) => {
    try {
        const comment = await prisma.comment.update({
            where: {
                id: commentId,
            },
            data,
        });

        return { result: comment, error: null };
    } catch (err) {
        return { result: null, error: err.message };
    }
};

// Delete a comment by ID
exports.deleteCommentById = async (commentId) => {
    try {
        const comment = await prisma.comment.delete({
            where: {
                id: commentId,
            },
        });

        return { result: comment, error: null };
    } catch (err) {
        return { result: null, error: err.message };
    }
};
