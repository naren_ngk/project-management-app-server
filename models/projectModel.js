const prisma = require('../prisma/prisma');

// Create a new project
exports.createProject = async (name, description, startDate, endDate, createdByUserId, assignedUserIds) => {
    try {
        const project = await prisma.project.create({
            data: {
                name,
                description,
                startDate,
                endDate,
                admin: { connect: { id: createdByUserId } },
                assignedUsers: { connect: assignedUserIds.map(userId => ({ id: userId })) },
            },
            include: {
                admin: true,
                assignedUsers: true
            }
        });

        return { result: project, error: null };
    } catch (err) {
        return { result: null, error: err };
    }
};

// Retrieve all projects
exports.getAllProjects = async () => {
    try {
        const projects = await prisma.project.findMany();
        return { result: projects, error: null };
    } catch (err) {
        return { result: null, error: err };
    }
};

// Retrieve a project by ID
exports.getProjectById = async (projectId) => {
    try {
        const project = await prisma.project.findUnique({
            where: {
                id: projectId,
            },
        });

        return { result: project, error: null };
    } catch (err) {
        return { result: null, error: err };
    }
};

// Update a project by ID
exports.updateProjectById = async (projectId, projectData) => {
    const { name, description, endDate } = projectData;
    
    try {
        const project = await prisma.project.update({
            where: {
                id: projectId,
            },
            data: {
                name, description, endDate
            }
        });

        return { result: project, error: null };
    } catch (err) {
        return { result: null, error: err };
    }
};

// Delete a project by ID
exports.deleteProjectById = async (projectId) => {
    try {
        const project = await prisma.project.delete({
            where: {
                id: projectId,
            },
        });

        return { result: project, error: null };
    } catch (err) {
        return { result: null, error: err };
    }
};
