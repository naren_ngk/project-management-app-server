const prisma = require('../prisma/prisma');

// Create a new user
exports.createUser = async (username, email, password, role) => {
    try {
        const user = await prisma.user.create({
            data: {
                username,
                email,
                password,
                role,
            },
        });

        return { result: user, error: null };
    } catch (err) {
        return { result: null, error: err };
    }
};

// Retrieve all users
exports.getAllUsers = async () => {
    try {
        const users = await prisma.user.findMany();
        return { result: users, error: null };
    } catch (err) {
        return { result: null, error: err };
    }
};

// Retrieve a user by ID
exports.getUserById = async (userId) => {
    try {
        const user = await prisma.user.findUnique({
            where: {
                id: userId,
            },
        });

        return { result: user, error: null };
    } catch (err) {
        return { result: null, error: err };
    }
};

// Retrieve a user by email
exports.getUserByEmail = async (email) => {
    try {
        const user = await prisma.user.findUnique({
            where: {
                email: email,
            },
        });

        return { result: user, error: null };
    } catch (err) {
        return { result: null, error: err };
    }
};

// Update a user by ID
exports.updateUserById = async (userId, data) => {
    try {
        const user = await prisma.user.update({
            where: {
                id: userId,
            },
            data,
        });

        return { result: user, error: null };
    } catch (err) {
        return { result: null, error: err };
    }
};

// Delete a user by ID
exports.deleteUserById = async (userId) => {
    try {
        const user = await prisma.user.delete({
            where: {
                id: userId,
            },
        });

        return { result: user, error: null };
    } catch (err) {
        return { result: null, error: err };
    }
};
