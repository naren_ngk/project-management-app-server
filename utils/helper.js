const logger = require('./logger')(module);

const handleError = (res, errorMsg, errorName, statusCode) => {
	logger.error(errorMsg);
	res.status(statusCode).json({
		status: `Failure : ${errorName} `,
		message: errorMsg,
	});
};

const handleSuccess = (res, data = {}, successMsg, statusCode = 200) => {
	logger.info(successMsg);
	res.status(statusCode).json({
		status: 'Success',
		message: successMsg,
		data: { ...data },
	});
};

module.exports = {
	handleError,
	handleSuccess,
};
