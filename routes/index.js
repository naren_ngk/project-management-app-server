const express = require("express");
const router = express.Router();
const { handleSuccess } = require("../utils/helper");

const v1Route = require("./v1");

router.get("/", (req, res) => {
  handleSuccess(res, "You Hit the API Route");
});

router.use("/v1", v1Route);

module.exports = router;
