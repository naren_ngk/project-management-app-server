# User Authentication Routes (`/api/v1`)

This section outlines the routes and their descriptions for user authentication within the application.

## Login Existing User

  - **Route:** POST /login
  - **Description:** Authenticates an existing user.
  - **Body Parameters:**
    - `userMail`: Email address of the user.
    - `password`: User's password.
  - **Handler:** loginUser
  - **Error Handling:** Handles errors related to user retrieval, incorrect password, and internal server errors.
  - **Success Response:** Sends a JWT token upon successful authentication.

## Register New User

  - **Route:** POST /register
  - **Description:** Registers a new user.
  - **Body Parameters:**
    - `userName`: Username of the new user.
    - `email`: Email address of the new user.
    - `password`: Password for the new user's account.
  - **Handler:** registerUser
  - **Error Handling:** Handles errors related to existing user, database operations, and internal server errors.
  - **Success Response:** Sends a success message with the details of the created user.

# Project Management Routes (`/api/v1/project`)

This section provides details for controllers responsible for managing projects within the application.

## Create New Project

- **Route:** POST /new
- **Description:** Creates a new project.
- **Body Parameters:**
  - `projectName`: Name of the project.
  - `description`: Description of the project.
  - `startDate`: Start date of the project.
  - `endDate`: End date of the project.
  - `createdByUserId`: ID of the user creating the project.
  - `assignedUserIds`: Array of IDs of users assigned to the project.
- **Handler:** createProject
- **Error Handling:** Handles errors related to user retrieval, user assignment, existing users, database operations, and internal server errors.
- **Success Response:** Sends a success message with the details of the created project.

## Update Project

- **Route:** PUT /:projectId
- **Description:** Updates an existing project.
- **Body Parameters:**
  - `projectName`: Name of the project.
  - `description`: Description of the project.
  - `endDate`: End date of the project.
- **Handler:** updateProject
- **Error Handling:** Handles errors related to project retrieval, database operations, and internal server errors.
- **Success Response:** Sends a success message with the details of the updated project.

## Delete Project

- **Route:** DELETE /:projectId
- **Description:** Deletes an existing project.
- **Body:** None
- **Handler:** deleteProject
- **Error Handling:** Handles errors related to project retrieval, database operations, and internal server errors.
- **Success Response:** Sends a success message upon successful deletion of the project.

# Work Item Management Routes (`/api/v1/project`)

Work Item Management encompasses the process of overseeing tasks or activities within a project.

## Create Work Item

- **Route:** POST /:projectId/workitem
- **Description:** Creates a new work item within a project.
- **Body Parameters:**
  - `title`: Title of the work item.
  - `description`: Description of the work item.
  - `priority`: Priority of the work item.
  - `statusId`: ID of the status of the work item.
  - `assignedUserIds`: Array of IDs of users assigned to the work item.
  - `userId`: ID of the user creating the work item.
  - `endDate`: End date of the work item.
- **Handler:** createWorkItem
- **Error Handling:** Handles errors related to project retrieval, user assignment, existing users, database operations, and internal server errors.
- **Success Response:** Sends a success message with the details of the created work item.

## Update Work Item

- **Route:** PUT /:projectId/workitem/:itemId
- **Description:** Updates an existing work item within a project.
- **Body Parameters:**
  - `title`: Title of the work item.
  - `description`: Description of the work item.
  - `endDate`: End date of the work item.
- **Handler:** updateWorkItem
- **Error Handling:** Handles errors related to work item retrieval, database operations, and internal server errors.
- **Success Response:** Sends a success message with the details of the updated work item.

## Update Work Item Assigned Users

- **Route:** PUT /:projectId/workitem/:itemId/users
- **Description:** Updates the assigned users of an existing work item within a project.
- **Body Parameters:**
  - `assignedUserIds`: Array of IDs of users assigned to the work item.
- **Handler:** updateWorkItemAssignedUsers
- **Error Handling:** Handles errors related to work item retrieval, database operations, and internal server errors.
- **Success Response:** Sends a success message with the details of the updated work item.

## Delete Work Item

- **Route:** DELETE /:projectId/workitem/:itemId
- **Description:** Deletes an existing work item within a project.
- **Body:** None
- **Handler:** deleteWorkItem
- **Error Handling:** Handles errors related to work item retrieval, database operations, and internal server errors.
- **Success Response:** Sends a success message upon successful deletion of the work item.

# Comment Management Routes (`/api/v1/project`)

Comment management involves handling interactions and updates related to comments within the application. Here's an overview of key functionalities:

## Create Comment

- **Route:** POST /:projectId/workitem/:itemId/comment/new
- **Description:** Creates a new comment on a specific work item.
- **Body Parameters:**
  - `content`: Content of the comment.
  - `userId`: ID of the user creating the comment.
  - `workItemId`: ID of the work item the comment is associated with.
- **Handler:** createComment
- **Error Handling:** Handles errors related to user retrieval, work item retrieval, database operations, and internal server errors.
- **Success Response:** Sends a success message with the details of the created comment.

## Update Comment

- **Route:** PUT /:projectId/workitem/:itemId/comment/:commentId
- **Description:** Updates an existing comment by its ID.
- **URL Parameters:**
  - `commentId`: ID of the comment to be updated.
- **Body Parameters:**
  - `content`: Updated content of the comment.
- **Handler:** updateCommentById
- **Error Handling:** Handles errors related to comment retrieval, database operations, and internal server errors.
- **Success Response:** Sends a success message with the updated comment details.

## Delete Comment

- **Route:** DELETE /:projectId/workitem/:itemId/comment/:commentId
- **Description:** Deletes a comment by its ID.
- **URL Parameters:**
  - `commentId`: ID of the comment to be deleted.
- **Handler:** deleteCommentById
- **Error Handling:** Handles errors related to comment retrieval, database operations, and internal server errors.
- **Success Response:** Sends a success message indicating the deletion of the comment.