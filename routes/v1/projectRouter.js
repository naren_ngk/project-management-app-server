const express = require('express');

const projectValidation = require('../../validations/projectValidations');
const projectController = require('../../controllers/projectControllers');
const auth = require('../../middlewares/tokenAuth');
const roleChecker = require('../../middlewares/userRoleChecker');

const router = express.Router();

router
    .route('/new')
    .post(auth.protectedRoute, roleChecker('ADMIN'), projectValidation.createProjectValidation, projectController.createProject);

router
    .route('/:projectId')
    .put(auth.protectedRoute, projectValidation.updateProjectValidation, projectController.updateProject)
    .delete(auth.protectedRoute, projectController.deleteProject);

module.exports = router;