const express = require("express");
const router = express.Router();
const { handleSuccess } = require("../../utils/helper");

const authRouter = require('./authRouter');
const projectRouter = require('./projectRouter');
const workItemRouter = require('./workItemRouter');
const commentRouter = require('./commentRouter');

router.get('/', (req, res) => {
   handleSuccess(res, "You Hit API V1 Route");
});

router.use('/', authRouter);
router.use('/project', projectRouter);
router.use('/project', workItemRouter);
router.use('/project', commentRouter);

module.exports = router;
