const express = require('express');

const workItemValidation = require('../../validations/workItemValidations');
const workItemController = require('../../controllers/workItemController');
const auth = require('../../middlewares/tokenAuth');
const roleChecker = require('../../middlewares/userRoleChecker');

const router = express.Router();

router
    .route('/:projectId/workitem/new')
    .post(auth.protectedRoute, workItemValidation.createWorkItemValidation, workItemController.createWorkItem);
    

router
    .route('/:projectId/workitem/:itemId')
    .put(auth.protectedRoute, workItemValidation.updateWorkItemValidation, workItemController.updateWorkItem)
    .delete(auth.protectedRoute, workItemController.deleteWorkItem);

router
    .route('/:projectId/workitem/:itemId/users')
    .put(auth.protectedRoute, workItemValidation.updateWorkItemUserValidation, workItemController.updateWorkItemAssignedUsers);

module.exports = router;