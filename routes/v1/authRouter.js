const express = require('express');

const userValidation = require('../../validations/UserValidations');
const userController = require('../../controllers/userControllers');

const router = express.Router();

router
    .route('/register')
    .post(userValidation.signUpValidation, userController.registerUser);

router
    .route('/login')
    .post(userValidation.signInValidation, userController.loginUser);

module.exports = router;