const express = require('express');

const commentValidation = require('../../validations/commentValidations');
const commentController = require('../../controllers/commentsControllers');
const auth = require('../../middlewares/tokenAuth');

const router = express.Router();

router
    .route('/:projectId/workitem/:itemId/comment/new')
    .post(auth.protectedRoute, commentValidation.createCommentValidation, commentController.createComment);

router
    .route('/:projectId/workitem/:itemId/comment/:commentId')
    .put(auth.protectedRoute, commentValidation.updateCommentValidation, commentController.updateCommentById)
    .delete(auth.protectedRoute, commentController.deleteCommentById);

module.exports = router;